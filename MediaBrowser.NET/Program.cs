﻿using Microsoft.Extensions.Logging;

using uk.osric.mediabrowser;

namespace MediaBrowser.NET;

internal class Program
{
    private static string Indent(int depth) => new(' ', depth * 2);

    private static async Task Main(string[] args)
    {
        ILoggerFactory loggerFactory = LoggerFactory.Create(b =>
        {
            b.AddConsole();
            b.AddFilter(l => true);
        });

        using Pvr pvr = new(loggerFactory.CreateLogger<Pvr>());
        await pvr.Scan();

        if (pvr.RootFolder.GetRemoteFile("/media/My Video/Unwind With ITV/Unwind With ITV_20230105_0352.ts") is RemoteFile rf)
        {
            pvr.UnlockAsync(rf);
        }

        PrintFolder(pvr.RootFolder);
    }

    private static void PrintFolder(RemoteFolder folder, int depth = 0)
    {
        foreach (RemoteItem item in folder.Children.OrderBy(i => i is RemoteFolder).ThenBy(i => i.FtpPath))
        {
            Console.Write(Indent(depth));
            Console.WriteLine(item.ToString());
            if (item is RemoteFolder f)
            {
                PrintFolder(f, depth + 1);
            }
        }
    }
}

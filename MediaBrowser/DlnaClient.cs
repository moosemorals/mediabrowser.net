﻿using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using Microsoft.Extensions.Logging;

using Rssdp;

namespace uk.osric.mediabrowser;

internal class DlnaClient
{
    internal DlnaClient(ILogger<Pvr> log, HttpClient httpClient)
    {
        _httpClient = httpClient;
        _log = log;
    }

    internal Uri? ControlUri;

    private static IEnumerable<IPAddress> GetLocalAddresses => NetworkInterface.GetAllNetworkInterfaces()
        .SelectMany(ni => ni.GetIPProperties().UnicastAddresses.ToList())
        .Select(a => a.Address)
        .Where(a => a.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(a))
        .Where(a =>
        {
            byte[] addressBytes = a.GetAddressBytes();
            return addressBytes[0] != 0xA9 && addressBytes[1] != 0xFE;
        });

    private const string ContentDirectoryServiceType = "urn:schemas-upnp-org:service:ContentDirectory:1";

    private static readonly XNamespace dc = "http://purl.org/dc/elements/1.1/";
    private static readonly XNamespace didlLight = "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/";
    private static readonly XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";

    private static readonly Encoding utf8 = new UTF8Encoding(false);

    private static readonly XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

    private static readonly XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";

    private readonly HttpClient _httpClient;

    private readonly ILogger _log;

    internal async Task Scan(RemoteFolder root)
    {
        Stack<RemoteFolder> stack = new();
        stack.Push(root);
        while (stack.Count > 0)
        {
            RemoteFolder current = stack.Pop();
            if (!current.HasDlnaData)
            {
                _log.LogWarning("Folder missing Dlna data");
                continue;
            }

            XElement? result = await GetFolder(current.DlnaPath);
            if (result == null)
            {
                _log.LogWarning("Unable to get Dlna data for folder {DlnaFolder}", current.DlnaPath);
                continue;
            }

            foreach (XElement child in result.Elements())
            {
                string? path = child.Attribute("id")?.Value;
                string? filename = child.Element(dc + "title")?.Value;

                if (child.Name.LocalName == "container")
                {
                    stack.Push((RemoteFolder)current.AddChild(new RemoteFolder(current)
                    {
                        DlnaPath = path,
                        DlanFilename = filename,
                    }));
                }
                else if (child.Name.LocalName == "item")
                {
                    string? downloadUri = child.Element(didlLight + "res")?.Value;
                    _ = long.TryParse(child.Element(didlLight + "res")?.Attribute("size")?.Value, out long size);

                    current.AddChild(new RemoteFile(current)
                    {
                        DlnaPath = path,
                        DlanFilename = filename,
                        DownloadUri = downloadUri,
                        DnlaByteCount = size,
                    });
                }
            }
        }
    }

    internal async Task<IPAddress> ScanForPvr(string modelName)
    {
        IPAddress[] localIps = GetLocalAddresses.ToArray();

        int ipIndex = 0;
        while (true)
        {
            IPAddress localIp = localIps[ipIndex++];
            if (ipIndex == localIps.Length)
            {
                ipIndex = 0;
            }
            SsdpDeviceLocator locator = new(localIp.ToString());

            _log.LogInformation("Scanning for devices from {LocalIp}", localIp);
            foreach (DiscoveredSsdpDevice d in await locator.SearchAsync())
            {
                SsdpDevice device = await d.GetDeviceInfo();
                if (device.ModelName.StartsWith(modelName) && device.Services.FirstOrDefault(s => s.ServiceType == "ContentDirectory") is SsdpService service)
                {
                    _log.LogInformation("Found {ModelName} at {DeviceUri}", device.ModelName, d.DescriptionLocation);
                    ControlUri = new(d.DescriptionLocation, service.ControlUrl);
                    return localIp;
                }
                _log.LogDebug("Ignoring device {DeviceName}", device.ModelName);
            }
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
    }

    private static XElement BuildBrowseRequest(string path) =>
        SoapRequest(BrowseRequest.ForPath(path).ToXml());

    private static XElement SoapRequest(XElement payload)
        => new(soap + "Envelope",
               new XAttribute(XNamespace.Xmlns + "xsi", xsi),
               new XAttribute(XNamespace.Xmlns + "xsd", xsd),
               new XElement(soap + "Body", payload));

    private static string XmlToString(XElement xml)
    {
        MemoryStream stream = new();
        XmlWriterSettings settings = new()
        {
            Encoding = utf8,
            Indent = false,
            NamespaceHandling = NamespaceHandling.OmitDuplicates,
            OmitXmlDeclaration = false,
        };

        using (XmlWriter writer = XmlWriter.Create(stream, settings))
        {
            xml.Save(writer);
        }

        return utf8.GetString(stream.GetBuffer());
    }

    private async Task<XElement?> GetFolder(string path)
    {
        using (_log.BeginScope("GetFolder {Path}", path))
        {
            string request = XmlToString(BuildBrowseRequest(path));
            HttpRequestMessage httpRequest = new(HttpMethod.Post, ControlUri)
            {
                Content = new StringContent(request, utf8, "text/xml")
            };
            httpRequest.Headers.Add("SOAPAction", $"\"{ContentDirectoryServiceType}#Browse\"");

            _log.LogDebug("Making request for {DlnaBrowseUrl}", httpRequest.RequestUri);
            HttpResponseMessage httpResponse = await _httpClient.SendAsync(httpRequest);

            string body = await httpResponse.Content.ReadAsStringAsync();
            if (httpResponse.IsSuccessStatusCode)
            {
                string? result = (string?)XElement.Parse(body).Descendants().FirstOrDefault(n => n.Name == "Result");

                if (result == null)
                {
                    return null;
                }

                return XElement.Parse(result);
            }

            return null;
        }
    }
}

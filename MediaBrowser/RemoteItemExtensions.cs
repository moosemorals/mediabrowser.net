﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uk.osric.mediabrowser;
public static class RemoteItemExtensions
{
    public static RemoteFile? GetRemoteFile(this RemoteFolder folder, string path)
    {
        if (folder.HasFtpData)
        {
            if (folder.Files.FirstOrDefault(f => f.FtpPath == path) is RemoteFile result)
            {
                return result;
            }
            else if (folder.Folders.FirstOrDefault(f => f.HasFtpData && path.StartsWith(f.FtpPath)) is RemoteFolder step)
            {
                return GetRemoteFile(step, path);
            }
        }
        return null;
    }
}

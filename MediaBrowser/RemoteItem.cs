﻿using System.Diagnostics.CodeAnalysis;

namespace uk.osric.mediabrowser;

public class RemoteFile : RemoteItem
{
    public RemoteFile(RemoteFolder? parent) : base(parent) { }

    public string? Channel => HmtFile?.ChannelName;
    public string? Description => HmtFile?.Description;
    public long DnlaByteCount { get; internal set; }
    public string? DownloadUri { get; internal set; }
    public TimeSpan? Duration => HmtFile?.Duration;
    public DateTime? EndTime => HmtFile?.EndTime;
    public long FtpByteCount { get; internal set; }
    public bool? IsHighDef => HmtFile?.IsHighDef;
    public bool? IsLocked => HmtFile?.IsLocked;
    public string? MimeType { get; internal set; }
    public DateTime? StartTime => HmtFile?.StartTime;
    public string? Title => HmtFile?.Title;
    internal HmtFile? HmtFile { get; set; }

    public void Unlock() => HmtFile?.Unlock();
}

public abstract class RemoteItem
{
    public RemoteItem(RemoteFolder? parent)
    {
        Parent = parent;
    }

    public string? DlanFilename { get; internal set; }
    public string? DlnaPath { get; internal set; }
    public string? FtpFilename => FtpPath?.Split('/').Last();
    public string? FtpPath { get; internal set; }

    [MemberNotNullWhen(true, nameof(DlnaPath), nameof(DlanFilename))]
    public bool HasDlnaData => DlnaPath != null;

    [MemberNotNullWhen(true, nameof(FtpPath), nameof(FtpFilename))]
    public bool HasFtpData => FtpPath != null;

    public RemoteFolder? Parent { get; protected set; }

    public override string ToString()
    {
        return string.Join(", ", (new string?[] { FtpPath, DlnaPath, DlanFilename }).Where(s => s is not null));
    }
}

public class RemoteFolder : RemoteItem
{
    public RemoteFolder(RemoteFolder? parent) : base(parent) { }

    public IEnumerable<RemoteItem> Children => _children;
    public IEnumerable<RemoteFile> Files => Children.OfType<RemoteFile>();
    public IEnumerable<RemoteFolder> Folders => Children.OfType<RemoteFolder>();
    private readonly List<RemoteItem> _children = new();

    public RemoteItem AddChild(RemoteItem item)
    {
        if (item is RemoteFile file)
        {
            return AddChild(file);
        }
        else if (item is RemoteFolder folder)
        {
            return AddChild(folder);
        }
        else
        {
            throw new InvalidOperationException("Unknown remote item type");
        }
    }

    public RemoteItem AddChild(RemoteFile child)
    {
        if (ChildByDlnaFilename(child.FtpFilename) is RemoteFile ftpFile)
        {
            // Set FTP data on existing Dlna file
            ftpFile.FtpPath = child.FtpPath;
            ftpFile.FtpByteCount = child.FtpByteCount;
            return ftpFile;
        }
        else if (ChildByFtpFilename(child.DlanFilename) is RemoteFile dlnaFile)
        {
            // Set Dlna data on existing FTP file
            dlnaFile.DlnaPath = child.DlnaPath;
            dlnaFile.DlanFilename = child.DlanFilename;
            dlnaFile.DnlaByteCount = child.DnlaByteCount;
            dlnaFile.MimeType = child.MimeType;
            dlnaFile.DownloadUri = child.DownloadUri;
            return dlnaFile;
        }
        else
        {
            // Add it to the list
            _children.Add(child);
            return child;
        }
    }

    public RemoteItem AddChild(RemoteFolder child)
    {
        if (ChildByDlnaFilename(child.FtpFilename) is RemoteFolder ftpFolder)
        {
            ftpFolder.FtpPath = child.FtpPath;
            return ftpFolder;
        }
        else if (ChildByFtpFilename(child.DlanFilename) is RemoteFolder dlnaFolder)
        {
            dlnaFolder.DlnaPath = child.DlnaPath;
            dlnaFolder.DlanFilename = child.DlanFilename;
            return dlnaFolder;
        }
        else
        {
            // Add it to the list
            _children.Add(child);
            return child;
        }
    }

    private RemoteItem? ChildByDlnaFilename(string? filename) => filename != null ? _children.FirstOrDefault(f => f.DlanFilename == filename) : null;

    private RemoteItem? ChildByFtpFilename(string? filename) => filename != null ? _children.FirstOrDefault(f => f.FtpFilename == filename) : null;
}

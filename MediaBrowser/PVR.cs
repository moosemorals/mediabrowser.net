﻿using System.Net;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace uk.osric.mediabrowser;

public class Pvr : IDisposable
{
    public Pvr() : this(new NullLogger<Pvr>()) { }

    public Pvr(ILogger<Pvr> log)
    {
        _log = log;
    }

    public readonly RemoteFolder RootFolder = new(null)
    {
        FtpPath = "/media/My Video",
        DlnaPath = @"0\1\2",
    };

    private const string FtpPassword = "0000";
    private const string FtpUser = "humaxftp";
    private const string PvrModelName = "HDR-2000T";
    private readonly ILogger<Pvr> _log;
    private DlnaClient? _dlnaClient;
    private FtpClient? _ftpClient;

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        _ftpClient?.Dispose();
    }

    public async Task Scan()
    {
        _dlnaClient ??= new(_log, new HttpClient());

        IPAddress localIp = await _dlnaClient.ScanForPvr(PvrModelName);
        Uri? pvrUri = _dlnaClient.ControlUri;

        if (pvrUri == null)
        {
            throw new InvalidOperationException("Can't scan - No PVR control URI");
        }

        _ftpClient ??= new(_log, localIp, pvrUri.Host, FtpUser, FtpPassword);

        _ftpClient.Scan(RootFolder);
        await _dlnaClient.Scan(RootFolder);
    }

    public void UnlockAsync(RemoteFile file)
    {
        if (_ftpClient != null && _dlnaClient != null)
        {
            _ftpClient.Unlock(file); 
            return;
        }
        throw new IOException("Can't unlock file: Must scan before unlock");
    }
}

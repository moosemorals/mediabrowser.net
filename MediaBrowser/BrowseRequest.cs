﻿using System.Xml.Linq;

namespace uk.osric.mediabrowser;

internal class BrowseRequest
{
    internal BrowseRequest(string objectID, BrowseFlag browseFlag, string filter, int startingIndex, int requestedCount, string sortCriteria)
    {
        ObjectID = objectID;
        BrowseFlag = browseFlag;
        Filter = filter;
        StartingIndex = startingIndex;
        RequestedCount = requestedCount;
        SortCriteria = sortCriteria;
    }

    internal BrowseFlag BrowseFlag { get; init; }
    internal string Filter { get; init; }
    internal string ObjectID { get; init; }
    internal int RequestedCount { get; init; }
    internal string SortCriteria { get; init; }
    internal int StartingIndex { get; init; }
    internal const string SOAPAction = "urn:schemas-upnp-org:service:ContentDirectory:1#Browse";

    private static readonly XNamespace contentDirectory = "urn:schemas-upnp-org:service:ContentDirectory:1";

    internal static BrowseRequest ForPath(string path) => new(path, BrowseFlag.BrowseDirectChildren, "*", 0, 1000, "");

    internal XElement ToXml()
    {
        return new(contentDirectory + "Browse",
            new XElement(contentDirectory + "ObjectID", ObjectID),
            new XElement(contentDirectory + "BrowseFlag", BrowseFlag.ToString()),
            new XElement(contentDirectory + "Filter", Filter.ToString()),
            new XElement(contentDirectory + "StartingIndex", StartingIndex),
            new XElement(contentDirectory + "RequestedCount", RequestedCount),
            new XElement(contentDirectory + "SortCriteria", SortCriteria.ToString())
            );
    }
}

internal enum BrowseFlag
{
    BrowseMetadata,
    BrowseDirectChildren
}

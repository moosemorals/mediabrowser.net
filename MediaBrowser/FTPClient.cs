﻿using System.Net;
using System.Text;

using Microsoft.Extensions.Logging;

namespace uk.osric.mediabrowser;

/// <summary>
///
/// </summary>
internal class FtpClient : IDisposable
{
    internal FtpClient(ILogger<Pvr> log, IPAddress localAddress, string serverHost, string username, string password)
    {
        _serverHost = serverHost;
        _username = username;
        _password = password;
        _localAddress = localAddress;
        _log = log;
        _connection = new(_log);
    }

    private readonly FtpCommandConnection _connection;
    private readonly IPAddress _localAddress;
    private readonly int _localPort = 18542;
    private readonly ILogger _log;
    private readonly string _password;
    private readonly string _serverHost;
    private readonly string _username;

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        if (_connection.IsConnected)
        {
            Execute("QUIT", "221");
            _connection.Close();
        }
        _connection.Dispose();
    }

    public void Unlock(RemoteFile remoteFile)
    {
        if (Path.GetExtension(remoteFile.FtpPath) != ".ts")
        {
            throw new IOException("Can't unlock file: Not a .ts file");
        }

        if (!remoteFile.HasFtpData || remoteFile.HmtFile == null)
        {
            throw new IOException("Can't unlock file: No FTP data");
        }

        // Change the bytes locally
        remoteFile.Unlock();

        // Post the bytes to the FTP server
        byte[] hmtFile = remoteFile.HmtFile.GetBytes();
        string hmtPath = Path.ChangeExtension(remoteFile.FtpPath, ".hmt");
        ExecuteSTOR(hmtPath, hmtFile);

        // Trigger the remote server to rescan by changing the filename
        string oldPath = Path.GetDirectoryName(remoteFile.FtpPath) ?? throw new IOException("Can't unlock: FtpPath is null");
        string oldName = Path.GetFileNameWithoutExtension(remoteFile.FtpPath);
        string oldExtension = Path.GetExtension(remoteFile.FtpPath);
        string newPath;
        if (oldName.EndsWith('-'))
        {
            newPath = FixPath($"{oldPath}/{oldName[..-1]}{oldExtension}");
        }
        else
        {
            newPath = FixPath($"{oldPath}/{oldName}-{oldExtension}");
        }

        RenameRemote(remoteFile.FtpPath, newPath);
        RenameRemote(newPath, remoteFile.FtpPath);
    }

    internal void Scan(RemoteFolder root)
    {
        Connect();
        WalkTree(root);
    }

    private static string FixPath(string path)
    {
        if (Path.DirectorySeparatorChar != Path.AltDirectorySeparatorChar)
        {
            return path.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
        }
        return path;
    }

    private static RemoteItem? ParseListLine(RemoteFolder? parent, string line)
    {
        // -rw-r--r--   1 root root 3186298880 Dec 27 22:52 1917_20221227_2101.ts

        string[] parts = line.Split(' ', 9, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        if (parts.Length != 9)
        {
            return null;
        }

        string parentFolder = parent?.FtpPath ?? "";
        string filename = parts[ListParts.Filename];

        if (line[0] == 'd')
        {
            // Folder
            RemoteFolder folder = new(parent)
            {
                FtpPath = $"{parentFolder}/{filename}",
            };
            return folder;
        }
        else
        {
            if (!long.TryParse(parts[ListParts.Size], out long size))
            {
                return null;
            }

            return new RemoteFile(parent)
            {
                FtpPath = $"{parentFolder}/{filename}",
                FtpByteCount = size,
            };
        }
    }

    private void Connect()
    {
        if (_connection.IsConnected)
        {
            return;
        }

        _log.LogInformation("Opening FTP connection to {FtpServer}", _serverHost);
        _connection.Open(_localAddress, _localPort, _serverHost);

        _log.LogDebug("Connected to FTP server");

        Consume("220"); // Welcome response

        Execute($"USER {_username}", "331");
        Execute($"PASS {_password}", "230");
        ExecutePORT(_localAddress, _localPort);
    }

    private string Consume(params string[] expected)
    {
        string actual = _connection.ReadLine();
        foreach (string e in expected)
        {
            if (actual.StartsWith(e))
            {
                return actual.Substring(expected.Length + 1);
            }
        }
        throw new IOException($"Unexpected reply from FTP {actual}");
    }

    private string Execute(string cmd, string expected)
    {
        _log.LogDebug("Sending commmand {FtpCommand}", cmd);
        _connection.WriteLine(cmd);
        return Consume(expected);
    }

    private void ExecuteCWD(string path) => Execute($"CWD {path}", "250");

    private IEnumerable<string> ExecuteLIST()
    {
        Execute("LIST", "150");
        List<string> lines = new();
        using (StreamReader reader = new(_connection.ReceiveData(), Encoding.ASCII))
        {
            string? line;
            while ((line = reader.ReadLine()) != null)
            {
                lines.Add(line);
            }
            Consume("226");
            return lines;
        }
    }

    private void ExecutePORT(IPAddress local, int port)
    {
        string[] parts = new string[6];
        Array.Copy(local.ToString().Split('.'), parts, 4);
        parts[4] = ((port >> 8) & 0xff).ToString();
        parts[5] = (port & 0xff).ToString();
        _log.LogDebug("Sending PORT {LocalIPAddress}:{LocalPort}", local.ToString(), port);
        Execute($"PORT {string.Join(',', parts)}", "200");
    }

    private byte[] ExecuteRETR(string path)
    {
        Execute($"RETR {path}", "150");
        MemoryStream buffer = new();
        using (Stream remoteData = _connection.ReceiveData())
        {
            remoteData.CopyTo(buffer);
        }
        Consume("226");
        return buffer.GetBuffer();
    }

    private void ExecuteRNFR(string path) => Execute($"RNFR {path}", "350");

    private void ExecuteRNTO(string path) => Execute($"RNTO {path}", "250");

    private void ExecuteSTOR(string path, byte[] contents)
    {
        Execute($"STOR {path}", "150");
        _connection.SendData(contents);
        Consume("226");
    }

    private HmtFile GetHmtFile(string path) => new(ExecuteRETR(path));

    private void RenameRemote(string fromPath, string toPath)
    {
        ExecuteRNFR(fromPath);
        ExecuteRNTO(toPath);
    }

    private void WalkTree(RemoteFolder root)
    {
        if (_localAddress == null)
        {
            throw new InvalidOperationException("Can't walk tree: No local IP");
        }

        // Basic depth first search
        Stack<RemoteFolder> stack = new();
        stack.Push(root);
        while (stack.Count > 0)
        {
            RemoteFolder current = stack.Pop();
            if (current.HasFtpData)
            {
                _log.LogDebug("Scanning FTP folder {FtpPath}", current.FtpPath);
                ExecuteCWD(current.FtpPath);
            }
            else
            {
                _log.LogWarning("Can't change folder, missing ftp path");
                continue;
            }

            // Run remote LIST command
            foreach (string line in ExecuteLIST())
            {
                RemoteItem? item = ParseListLine(current, line);
                if (item == null || !item.HasFtpData)
                {
                    _log.LogWarning("Can't parse listing line '{ListLine}'", line);
                    continue;
                }
                else if (item.FtpFilename.StartsWith('.'))
                {
                    continue;
                }

                if (item is RemoteFolder folder)
                {
                    stack.Push((RemoteFolder)current.AddChild(folder));
                }
                else if (item.FtpFilename.EndsWith(".ts"))
                {
                    RemoteFile file = (RemoteFile)current.AddChild(item);

                    string hmtPath = Path.ChangeExtension(item.FtpPath, "hmt");
                    file.HmtFile = GetHmtFile(hmtPath);
                }
            }
        }
    }

    private static class ListParts
    {
        internal const int Filename = 8;
        internal const int Size = 4;
    }
}

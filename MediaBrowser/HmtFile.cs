﻿using System.Text;

namespace uk.osric.mediabrowser;

internal class HmtFile
{
    public HmtFile(byte[] bytes)
    {
        _file = new byte[bytes.Length];

        // Paranoia - make our own copy of the array
        Array.Copy(bytes, _file, bytes.Length);
    }

    public string ChannelName => GetString(0x045D, 10);
    public string Description => GetString(0x0517, 255);
    public TimeSpan Duration => EndTime - StartTime;
    public DateTime EndTime => GetDateTime(0x284);

    public string Filename => GetString(FilenameOffset, 512);
    public string Folder => GetString(0x0080, 512);
    public string GuidanceDescription => GetString(0x03E3, 77);
    public bool IsHighDef => IsByte(0x04BC, 0x02);
    public bool IsLocked => !IsByte(0x03DC, 0x04);
    public DateTime StartTime => GetDateTime(0x280);
    public string Title => GetString(0x029A, 512);
    private int FilenameOffset => IsByte(0x017F, 0) ? 0x0180 : 0x017F;

    private readonly byte[] _file;

    public byte[] GetBytes()
    {
        byte[] _copy = new byte[_file.Length];
        Array.Copy(_file, _copy, _file.Length);
        return _copy;
    }

    public void SetFilename(string newName)
    {
        byte[] newNameBytes = Encoding.UTF8.GetBytes(newName);

        int nameLength = Math.Min(newNameBytes.Length, 510);

        Array.Copy(newNameBytes, 0, _file, FilenameOffset, nameLength);

        for (int i = nameLength; i < 512; i += 1)
        {
            _file[FilenameOffset + i] = 0;
        }
    }

    public void Unlock()
    {
        _file[0x03DC] = 0x04;
    }

    private byte GetByte(int offset) => _file[offset];

    private DateTime GetDateTime(int offset)
    {
        long ts = 0;
        for (int i = 0; i < 4; i += 1)
        {
            ts += ((long)GetByte(offset + i) & 0xff) << (i * 8);
        }

        return DateTime.UnixEpoch.AddSeconds(ts);
    }

    private string GetString(int offset, int length)
    {
        int i = 0;
        while (i < length && i + offset < _file.Length && _file[offset + i] != 0)
        {
            i += 1;
        }

        string result = Encoding.UTF8.GetString(_file, offset, i);

        if (result.StartsWith("i7"))
        {
            return result.Substring(2);
        }
        return result;
    }

    private bool IsByte(int offset, byte value) => GetByte(offset) == value;
}

﻿using System.Net;
using System.Net.Sockets;
using System.Text;

using Microsoft.Extensions.Logging;

namespace uk.osric.mediabrowser;

internal class FtpCommandConnection : IDisposable
{
    internal FtpCommandConnection(ILogger log)
    {
        _commandSocket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        _dataSocket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        _log = log;
    }

    internal bool IsConnected => _commandSocket.Connected;

    private const string EOL = "\r\n";

    private readonly Socket _commandSocket;
    private readonly Socket _dataSocket;
    private readonly ILogger _log;
    private readonly byte[] _readBuffer = new byte[1024 * 8];
    private int _readBufferUsed = 0;

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        _dataSocket.Dispose();
        _commandSocket.Dispose();
    }

    /// <summary>
    /// Run a second socket to receive traffic from the server
    /// </summary>
    public Stream ReceiveData()
    {
        MemoryStream stream = new();
        byte[] buffer = new byte[1024];
        _log.LogDebug("Waiting for data connection");
        using (Socket client = _dataSocket.Accept())
        {
            int read;
            _log.LogDebug("Waiting for data on data connection");
            while ((read = client.Receive(buffer)) != 0)
            {
                stream.Write(buffer, 0, read);
            }
            client.Shutdown(SocketShutdown.Both);
        }
        stream.Position = 0;
        return stream;
    }

    public void SendData(byte[] data)
    {
        using (Socket client = _dataSocket.Accept())
        {
            int toWrite = data.Length;
            while (toWrite > 0)
            {
                int wrote = client.Send(data, data.Length - toWrite, toWrite, SocketFlags.None);
                if (wrote == 0)
                {
                    throw new IOException("Problem sending to Ftp Server");
                }
                toWrite -= wrote;
            }
            client.Shutdown(SocketShutdown.Send);
        }
    }

    internal void Close()
    {
        _commandSocket.Shutdown(SocketShutdown.Both);
        _commandSocket.Disconnect(false);

        _dataSocket.Close(0);
    }

    internal void Open(IPAddress localAddress, int localPort, string remoteHost, int remotePort = 21)
    {
        _dataSocket.Bind(new IPEndPoint(localAddress, localPort));
        _dataSocket.Listen();
        _commandSocket.Connect(remoteHost, remotePort);
    }

    internal string ReadLine()
    {
        int read;
        _log.LogDebug("Waiting for incoming data");
        while ((read = _commandSocket.Receive(_readBuffer, _readBufferUsed, _readBuffer.Length - _readBufferUsed, SocketFlags.None)) != 0)
        {
            for (int i = _readBufferUsed; i < _readBufferUsed + read; i += 1)
            {
                if (_readBuffer[i] == '\n')
                {
                    i += 1;
                    string line = Encoding.ASCII.GetString(_readBuffer, 0, i);
                    Array.Copy(_readBuffer, i, _readBuffer, 0, _readBufferUsed + read - i);
                    _readBufferUsed = _readBufferUsed + read - i;
                    _log.LogDebug("Received line from ftp {FtpReceivedLine}", line);
                    return line;
                }
            }
            _readBufferUsed += read;
        }

        return string.Empty;
    }

    internal void WriteLine(string text)
    {
        _log.LogDebug("Sending ftp command {FtpCommand}", text);
        byte[] messageBytes = Encoding.ASCII.GetBytes(string.Concat(text, EOL));

        int toSend = messageBytes.Length;

        int sent;
        while ((sent = _commandSocket.Send(messageBytes, messageBytes.Length - toSend, toSend, SocketFlags.None)) != 0)
        {
            toSend -= sent;
        }

        if (toSend != 0)
        {
            throw new IOException($"Couldn't send complete message: {toSend} bytes remaining");
        }
    }
}
